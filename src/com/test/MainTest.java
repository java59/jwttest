package com.test;

import java.util.HashMap;

import com.util.JWTUtil;

import io.jsonwebtoken.Claims;

public class MainTest {
	
	public static void main(String[] args) throws Exception{
		JWTUtil util = new JWTUtil();
		
		HashMap<String, String> mapData = new HashMap<String, String>();
		mapData.put("TEST01", "MAMEE EIEI");
		mapData.put("TEST02", "MALEE EIEI");

		String tokenResult = util.enCode(mapData);
		System.out.println("tokenResult :"+tokenResult);
	
		Claims claimsResult = util.deCode(tokenResult);
		System.out.println("claimsResult :"+claimsResult);	
    }	
}