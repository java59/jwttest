package com.util;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;

public class JWTUtil {
	public String enCode(HashMap<String, String> mapData ) throws ParseException {
		String token = null;
		Date exp = PemUtil.getExpirationTime(Constan.EXPIRATION);
		System.out.println("exp :"+exp);
		Map<String, Object> header = new HashMap<String, Object>();
		header.put("JWT", "typ");
		Map<String, Object> claims = new HashMap<String, Object>();
		mapData.forEach((k,v)->claims.put(k,v));
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.RS512;
		try{
			PrivateKey privateKey = PemUtil.readPrivateKeyFromFile(Constan.PATH_PRIVATE_KEY, Constan.ALGORITHM_RSA);		
			token = Jwts.builder()
					.setHeader(header)
					.setClaims(claims)
					.setId(Constan.ID_JTI)
					.setExpiration(exp)
					.signWith(signatureAlgorithm, privateKey)
					.compact();
		}catch(Exception ex){
			System.out.println("ERROR MAGSAG :"+ex.getMessage());
			ex.printStackTrace();	
		}
		return token;
	}
	
	public Claims deCode(String token)throws Exception{
		Claims claims = null;
		try{
			PublicKey publicKey = PemUtil.readPublicKeyFromFile(Constan.PATH_PUBLIC_KEY,Constan.ALGORITHM_RSA);
			claims = Jwts.parser().setSigningKey(publicKey).parseClaimsJws(token).getBody();
//			System.out.println("Header :"+Jwts.parser().setSigningKey(publicKey).parseClaimsJws(token).getHeader());
//			System.out.println("Body :"+Jwts.parser().setSigningKey(publicKey).parseClaimsJws(token).getBody());
//			System.out.println("ToKen :"+Jwts.parser().setSigningKey(publicKey).parseClaimsJws(token).getSignature().toString());		
		}catch (ExpiredJwtException expired) {
			throw expired;
		} catch (MalformedJwtException malformedJwt) {
			throw malformedJwt;
		} catch (SignatureException signature) {
			throw signature;
		} catch (Exception e) {
			throw e;
		}
		return claims;
	}
}
